﻿Imports System.IO

Module Module1

    Sub Main()
        Dim folderLocations As String = "C:\EF\"
        Dim folderEntries As String() = Directory.GetDirectories(folderLocations)

        For Each folder In folderEntries
            Dim currentFile As String() = Directory.GetFiles(folder)
            For Each file In currentFile
                addRoute(file)
            Next
        Next

        Console.WriteLine("Complete")
        Console.ReadLine()


    End Sub

    Public Sub addRoute(ByVal fileName As String)

        Dim routerMod As String = ""
        Dim reader = File.OpenText(fileName)
        Dim mprFile As String = ""
        While (reader.Peek() <> -1)
            mprFile = reader.ReadToEnd()
        End While

        reader.Close()

        mprFile = mprFile.Replace("ZP500_ENU.mpr", "route.mpr")

        Using sink As New StreamWriter(fileName)
            sink.WriteLine(mprFile)
        End Using

        Console.WriteLine(fileName & "Done")

    End Sub

End Module
